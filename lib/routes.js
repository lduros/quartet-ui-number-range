"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _PoolList = require("./components/PoolList");

var _RegionDetail = require("./components/RegionDetail");

var _AddRegion = require("./components/AddRegion");

var _AddRandomizedRegion = require("./components/AddRandomizedRegion");

var _AddListBasedRegion = require("./components/AddListBasedRegion");

var _AddPool = require("./components/AddPool");

var _AddResponseRule = require("./components/AddResponseRule");

const React = qu4rtet.require("react"); // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const { Route } = qu4rtet.require("react-router");

/**
 * Default - Returns an array of routes to be appended to main Switch.
 *
 * @return {array} An array of routes to be appended to main app.
 */

exports.default = (() => {
  return [React.createElement(Route, {
    key: "poolList",
    path: "/number-range/pools/:serverID",
    component: _PoolList.PoolList
  }), React.createElement(Route, {
    key: "regionDetail",
    path: "/number-range/region-detail/:serverID/:pool",
    component: _RegionDetail.RegionDetail
  }), React.createElement(Route, {
    key: "addRegion",
    path: "/number-range/add-region/:serverID/:pool",
    component: _AddRegion.AddRegion
  }), React.createElement(Route, {
    key: "addRandomRegion",
    path: "/number-range/add-randomized-region/:serverID/:pool",
    component: _AddRandomizedRegion.AddRandomizedRegion
  }), React.createElement(Route, {
    key: "addListBasedRegion",
    path: "/number-range/add-list-based-region/:serverID/:pool",
    component: _AddListBasedRegion.AddListBasedRegion
  }), React.createElement(Route, {
    key: "editRegion",
    path: "/number-range/edit-region/:serverID/:pool",
    component: _AddRegion.AddRegion
  }), React.createElement(Route, {
    key: "editRandomRegion",
    path: "/number-range/edit-randomized-region/:serverID/:pool",
    component: _AddRandomizedRegion.AddRandomizedRegion
  }), React.createElement(Route, {
    key: "editListBasedRegion",
    path: "/number-range/edit-list-based-region/:serverID/:pool",
    component: _AddListBasedRegion.AddListBasedRegion
  }), React.createElement(Route, {
    key: "addPool",
    path: "/number-range/add-pool/:serverID/:poolName?",
    component: _AddPool.AddPool
  }), React.createElement(Route, {
    key: "editPool",
    path: "/number-range/edit-pool/:serverID/:poolName?",
    component: _AddPool.AddPool
  }), React.createElement(Route, {
    key: "addResponseRule",
    path: "/number-range/add-response-rule/:serverID/pool-id/:poolID",
    component: _AddResponseRule.AddResponseRule
  })];
})();