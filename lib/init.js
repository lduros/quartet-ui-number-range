"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.disablePlugin = exports.enablePlugin = undefined;

var _NavItems = require("./components/NavItems");

var _routes = require("./routes");

var _routes2 = _interopRequireDefault(_routes);

var _numberrange = require("./reducers/numberrange");

var _numberrange2 = _interopRequireDefault(_numberrange);

var _messages = require("./messages");

var _messages2 = _interopRequireDefault(_messages);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
const actions = qu4rtet.require("./actions/plugins").default;
const { pluginRegistry } = qu4rtet.require("./plugins/pluginRegistration");
const PLUGIN_NAME = "NumberRange";

var fs = require("fs");

/* At runtime, read css as text */
require.extensions[".css"] = function (module, filename) {
  module.exports = fs.readFileSync(filename, "utf-8");
};

const enablePlugin = exports.enablePlugin = () => {
  // setTimeout(() => {
  pluginRegistry.registerReducer(PLUGIN_NAME, "numberrange", _numberrange2.default, (0, _numberrange.initialData)());
  pluginRegistry.setMessages(_messages2.default);
  pluginRegistry.registerRoutes(PLUGIN_NAME, _routes2.default);
  pluginRegistry.registerComponent(PLUGIN_NAME, _NavItems.NavPluginRoot, actions.addToTreeServers);
  pluginRegistry.registerCss(PLUGIN_NAME, require("./style.css"));
  store.dispatch({ type: "PLUGINS_PLUGIN_LIST_UPDATED", payload: true });
  //}, 100);
};

const disablePlugin = exports.disablePlugin = () => {
  pluginRegistry.unregisterRoutes(PLUGIN_NAME);
  pluginRegistry.unregisterComponent(PLUGIN_NAME, _NavItems.NavPluginRoot, actions.removeFromTreeServers);
  pluginRegistry.unregisterCss(PLUGIN_NAME);
};