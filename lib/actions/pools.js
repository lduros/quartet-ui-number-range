"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
const { createAction } = qu4rtet.require("redux-actions");

exports.default = {
  loadPools: createAction("NUMBER_RANGE_LOAD_POOLS"),
  loadRegion: createAction("NUMBER_RANGE_LOAD_REGION"),
  loadRegions: createAction("NUMBER_RANGE_LOAD_REGIONS"),
  allocate: createAction("NUMBER_RANGE_ALLOCATE")
};