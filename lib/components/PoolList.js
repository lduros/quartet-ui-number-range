"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PoolList = undefined;

var _numberrange = require("../reducers/numberrange");

const React = qu4rtet.require("react"); // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const { Component } = React;
const { connect } = qu4rtet.require("react-redux");
const { RightPanel } = qu4rtet.require("./components/layouts/Panels");
const { Card } = qu4rtet.require("@blueprintjs/core");
const { FormattedMessage, FormattedDate, FormattedNumber } = qu4rtet.require("react-intl");
const { Link } = qu4rtet.require("react-router-dom");
const { pluginRegistry } = qu4rtet.require("./plugins/pluginRegistration");

class ServerPools extends Component {
  render() {
    let serverName = this.props.server.serverSettingName;
    let serverID = this.props.server.serverID;
    return React.createElement(
      Card,
      { className: "pt-elevation-4" },
      React.createElement(
        "h5",
        null,
        React.createElement(
          "button",
          {
            className: "pt-button add-pool-button pt-intent-primary",
            onClick: e => {
              this.props.history.push(`/number-range/add-pool/${serverID}/`);
            } },
          React.createElement(FormattedMessage, { id: "plugins.numberRange.addPool" })
        ),
        serverName
      ),
      React.createElement("div", null),
      React.createElement(
        "div",
        null,
        React.createElement(
          "table",
          { className: "pool-list-table pt-table pt-bordered pt-striped" },
          React.createElement(
            "thead",
            null,
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                React.createElement(FormattedMessage, {
                  id: "plugins.numberRange.createdOn",
                  defaultMessage: "Created on"
                })
              ),
              React.createElement(
                "th",
                null,
                React.createElement(FormattedMessage, {
                  id: "plugins.numberRange.readableName",
                  defaultMessage: "Readable Name"
                })
              ),
              React.createElement(
                "th",
                null,
                React.createElement(FormattedMessage, {
                  id: "plugins.numberRange.machineName",
                  defaultMessage: "Machine Name"
                })
              ),
              React.createElement(
                "th",
                null,
                React.createElement(FormattedMessage, {
                  id: "plugins.numberRange.status",
                  defaultMessage: "Status"
                })
              ),
              React.createElement(
                "th",
                null,
                React.createElement(FormattedMessage, {
                  id: "plugins.numberRange.requestThreshold",
                  defaultMessage: "Request Threshold"
                })
              ),
              React.createElement(
                "th",
                null,
                React.createElement(FormattedMessage, {
                  id: "plugins.numberRange.regions",
                  defaultMessage: "Regions"
                })
              )
            )
          ),
          React.createElement(
            "tbody",
            null,
            Array.isArray(this.props.pools) && this.props.pools.length > 0 ? this.props.pools.map(pool => {
              let regionNumber = 0;
              if (Number(pool.sequentialregion_set.length)) {
                regionNumber = Number(pool.sequentialregion_set.length);
              }
              if (pool.randomizedregion_set && Number(pool.randomizedregion_set.length)) {
                regionNumber += Number(pool.randomizedregion_set.length);
              }
              if (pool.listbasedregion_set && Number(pool.listbasedregion_set.length)) {
                regionNumber += Number(pool.listbasedregion_set.length);
              }
              return React.createElement(
                "tr",
                { key: pool.machine_name },
                React.createElement(
                  "td",
                  null,
                  React.createElement(FormattedDate, {
                    value: pool.created_date,
                    day: "numeric",
                    month: "long",
                    year: "numeric"
                  })
                ),
                React.createElement(
                  "td",
                  null,
                  pool.readable_name
                ),
                React.createElement(
                  "td",
                  null,
                  React.createElement(
                    Link,
                    {
                      to: `/number-range/region-detail/${serverID}/${pool.machine_name}` },
                    pool.machine_name
                  )
                ),
                React.createElement(
                  "td",
                  null,
                  pool.active ? React.createElement(FormattedMessage, {
                    id: "plugins.numberRange.active",
                    defaultMessage: "active"
                  }) : React.createElement(FormattedMessage, {
                    id: "plugins.numberRange.inactive",
                    defaultMessage: "inactive"
                  })
                ),
                React.createElement(
                  "td",
                  null,
                  React.createElement(FormattedNumber, { value: pool.request_threshold })
                ),
                React.createElement(
                  "td",
                  null,
                  React.createElement(
                    Link,
                    {
                      to: `/number-range/region-detail/${serverID}/${pool.machine_name}/` },
                    regionNumber,
                    " ",
                    React.createElement(FormattedMessage, {
                      id: "plugins.numberRange.regions",
                      defaultMessage: "regions"
                    })
                  )
                )
              );
            }) : null
          )
        )
      )
    );
  }
}

class _PoolList extends Component {
  componentDidMount() {
    let { server } = this.props;
    this.props.loadPools(pluginRegistry.getServer(server.serverID));
  }
  render() {
    let { server, pools } = this.props;
    return React.createElement(
      RightPanel,
      {
        title: React.createElement(FormattedMessage, {
          id: "plugins.numberRange.numberRangePools",
          defaultMessage: "Number Range Pools"
        }) },
      React.createElement(
        "div",
        { className: "large-cards-container" },
        React.createElement(ServerPools, {
          history: this.props.history,
          server: server,
          pools: pools
        })
      )
    );
  }
}

var PoolList = exports.PoolList = connect((state, ownProps) => {
  return {
    server: state.serversettings.servers[ownProps.match.params.serverID],
    pools: state.numberrange.servers ? state.numberrange.servers[ownProps.match.params.serverID].pools : []
  };
}, { loadPools: _numberrange.loadPools })(_PoolList);