"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AddResponseRule = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var _numberrange = require("../reducers/numberrange");

var _RuleDialog = require("./Dialogs/RuleDialog");

const React = qu4rtet.require("react");
const { Component } = React;
const { connect } = qu4rtet.require("react-redux");
const { RightPanel } = qu4rtet.require("./components/layouts/Panels");
const { Card } = qu4rtet.require("@blueprintjs/core");
const { FormattedMessage } = qu4rtet.require("react-intl");
const { pluginRegistry } = qu4rtet.require("./plugins/pluginRegistration");
const PageForm = qu4rtet.require("./components/elements/PageForm").default;
const { Field, reduxForm, SubmissionError, change } = qu4rtet.require("redux-form");
const loadRules = qu4rtet.require("./plugins/capture/src/reducers/capture");


const ResponseRuleForm = reduxForm({
  form: "responseRuleForm"
})(PageForm);

class _AddResponseRule extends Component {
  constructor(props) {

    super(props);

    this.toggleRuleDialog = evt => {
      this.setState({ isRuleOpen: !this.state.isRuleOpen });
    };

    this.state = {
      formStructure: [],
      isRuleOpen: false
    };
  }
  submitCallback() {
    this.props.loadPools(this.props.server);
  }

  render() {
    const editMode = !!(this.props.location && this.props.location.state && this.props.location.state.edit);
    let responseRule = null;
    if (this.props.location && this.props.location.state && this.props.location.state.defaultValues) {
      responseRule = this.props.location.state.defaultValues;
    } else {
      responseRule = {};
    }
    const pool = this.props.location.state.pool;

    return React.createElement(
      RightPanel,
      {
        title: !editMode ? React.createElement(FormattedMessage, { id: "plugins.numberRange.addResponseRule" }) : React.createElement(FormattedMessage, { id: "plugins.numberRange.editResponseRule" })
      },
      React.createElement(
        "div",
        { className: "large-cards-container" },
        React.createElement(
          Card,
          { className: "pt-elevation-4 form-card" },
          React.createElement(
            "h5",
            null,
            !editMode ? React.createElement(FormattedMessage, { id: "plugins.numberRange.addResponseRule" }) : React.createElement(FormattedMessage, { id: "plugins.numberRange.editResponseRule" })
          ),
          React.createElement(
            "div",
            { style: { textAlign: "center" } },
            React.createElement(
              "span",
              null,
              pool.readable_name
            )
          ),
          React.createElement(ResponseRuleForm, {
            edit: editMode,
            operationId: editMode ? "serialbox_response_rules_update" : "serialbox_response_rules_create",
            objectName: "Response Rule",
            submitCallback: this.submitCallback.bind(this),
            redirectPath: `/number-range/edit-pool/${this.props.server.serverID}/${pool.machine_name}`,
            djangoPath: "serialbox/response-rules/",
            existingValues: responseRule,
            prepopulatedValues: [{ name: "pool", value: pool.id }],
            parameters: responseRule ? { id: responseRule.id } : {},
            fieldElements: {
              rule: React.createElement(_RuleDialog.RuleDialog, _extends({}, this.props, {
                server: this.props.server,
                formName: "responseRuleForm",
                changeFieldValue: this.props.change,
                isRuleOpen: this.state.isRuleOpen,
                existingValues: responseRule,
                loadEntries: this.props.rules ? () => {} : this.props.loadRules,
                toggleRuleDialog: this.toggleRuleDialog,
                entries: this.props.rules || []
              }))
            },
            server: pluginRegistry.getServer(this.props.server.serverID),
            history: this.props.history
          })
        )
      )
    );
  }
}

const AddResponseRule = exports.AddResponseRule = connect((state, ownProps) => {
  return {
    server: state.serversettings.servers[ownProps.match.params.serverID],
    rules: state.capture.servers ? state.capture.servers[ownProps.match.params.serverID].rules : [],
    theme: state.layout.theme
  };
}, { loadRules, change, loadPools: _numberrange.loadPools })(_AddResponseRule);